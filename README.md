This is Weather Service with implemented OpenWeatherService client

System Requirements
  -------------------

  JDK:
    8

  Maven:
    3.6 or above


##For run service in command line execute next line:
   ---------------
   mvn clean spring-boot:run
   

## Then service provide two operations:
------

##  Retrieve Temperature(°C) for city by code
    -------------
    method = GET
	http://{SERVICE IP ADDRESS}:8080/weather/city/{id}
	
	where 
	 id - id of  city (1- Berlin; 2 - Minsk)

	 
## Add a new city
   --------------------
     method = POST
    http://{SERVICE IP ADDRESS}:8080/weather/city/
	send request like this:
	{
     "id": "CITY_ID",
     "name": "CITY_NAME"
    }
    
    where
     CITY_ID   - city code 
     CITY_NAME -  city name
     
## Api Documentation available by next URL
  ------------------------ 
   
   http://{SERVICE IP ADDRESS}:8080/swagger-ui.html