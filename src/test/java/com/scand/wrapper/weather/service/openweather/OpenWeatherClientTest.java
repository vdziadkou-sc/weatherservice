package com.scand.wrapper.weather.service.openweather;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.scand.wrapper.weather.service.openweather.dto.OpenWeatherMainDto;
import com.scand.wrapper.weather.service.openweather.dto.OpenWeatherResponseDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(SpringExtension.class)
@RestClientTest(OpenWeatherClient.class)
@TestPropertySource(properties = "app.weather.api.key=key-123")
public class OpenWeatherClientTest {

    private static final String URL = "http://api.openweathermap.org/data/2.5/";

    @Value("${app.weather.api.key}")
    String apiKey;

    private OpenWeatherClient weatherService;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() throws Exception {
        RestTemplate restTemplate = restTemplateBuilder.build();
        server = MockRestServiceServer.createServer(restTemplate);

        weatherService = new OpenWeatherClient(restTemplate);
        weatherService.setApiKey(apiKey);

        OpenWeatherMainDto weatherMain = new OpenWeatherMainDto();

        weatherMain.setTemp(new BigDecimal("278.15"));
        weatherMain.setTempMax(new BigDecimal("278.15"));
        weatherMain.setTempMin(new BigDecimal("278.15"));
        weatherMain.setPressure(new BigDecimal("997"));
        weatherMain.setFeelsLike(new BigDecimal("270.01"));
        weatherMain.setHumidity(new BigDecimal("75"));

        OpenWeatherResponseDto weatherResponse = new OpenWeatherResponseDto();
        weatherResponse.setId(1L);
        weatherResponse.setMain(weatherMain);

        String weatherResponseString =
                objectMapper.writeValueAsString(weatherResponse);

        server.expect(requestTo(URL + "weather?q=Minsk&APPID=key-123"))
                .andRespond(withSuccess(weatherResponseString, MediaType.APPLICATION_JSON));

        this.server.expect(requestTo(URL + "weather?id=625144&APPID=key-123"))
                .andRespond(withSuccess(weatherResponseString, MediaType.APPLICATION_JSON));
    }

    @WithMockUser("USER")
    @Test
    public void getWeatherByCityURL() {
        OpenWeatherResponseDto weather = weatherService.getWeather("Minsk");

        assertThat(weather.getId()).isEqualTo(1);

        OpenWeatherMainDto actualWeatherMain = weather.getMain();

        verifyWeatherResponse(actualWeatherMain);

    }

    @Test
    public void getWeatherByCityCode() {
        OpenWeatherResponseDto weather = weatherService.getWeather("Minsk");

        assertThat(weather.getId()).isEqualTo(1);

        OpenWeatherMainDto actualWeatherMain = weather.getMain();

        verifyWeatherResponse(actualWeatherMain);
    }

    private void verifyWeatherResponse(OpenWeatherMainDto actualWeatherMain) {
        assertThat(actualWeatherMain.getTemp()).isEqualTo(new BigDecimal("278.15"));
        assertThat(actualWeatherMain.getTempMax()).isEqualTo(new BigDecimal("278.15"));
        assertThat(actualWeatherMain.getTempMin()).isEqualTo(new BigDecimal("278.15"));
        assertThat(actualWeatherMain.getPressure()).isEqualTo(new BigDecimal("997"));
        assertThat(actualWeatherMain.getFeelsLike()).isEqualTo(new BigDecimal("270.01"));
        assertThat(actualWeatherMain.getHumidity()).isEqualTo(new BigDecimal("75"));
    }

}