package com.scand.wrapper.weather.service;

import com.scand.wrapper.weather.model.City;
import com.scand.wrapper.weather.model.Weather;
import com.scand.wrapper.weather.service.openweather.OpenWeatherClient;
import com.scand.wrapper.weather.service.openweather.dto.OpenWeatherMainDto;
import com.scand.wrapper.weather.service.openweather.dto.OpenWeatherResponseDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class WeatherServiceTest {

    private OpenWeatherResponseDto weatherResponse;
    private OpenWeatherMainDto weatherMain;

    OpenWeatherClient openWeatherClient;

    WeatherService service;

    @Test
    public void getWeatherByCityCode() {

        openWeatherClient = Mockito.mock(OpenWeatherClient.class);

        weatherMain = new OpenWeatherMainDto();

        weatherMain.setTemp(new BigDecimal("278.15"));
        weatherMain.setTempMax(new BigDecimal("278.15"));
        weatherMain.setTempMin(new BigDecimal("278.15"));
        weatherMain.setPressure(new BigDecimal("997"));
        weatherMain.setFeelsLike(new BigDecimal("270.01"));
        weatherMain.setHumidity(new BigDecimal("75"));

        weatherResponse = new OpenWeatherResponseDto();
        weatherResponse.setId(1L);
        weatherResponse.setMain(weatherMain);

        when(openWeatherClient.getWeather(625144L)).thenReturn(weatherResponse);

        service = new WeatherService(openWeatherClient);

        City city = new City(2, "Minsk", 625144L);
        verifyWeatherResponse(service.getWeather(city));
    }

    @Test
    public void getIdByCityName() {

        openWeatherClient = Mockito.mock(OpenWeatherClient.class);

        weatherMain = new OpenWeatherMainDto();

        weatherMain.setTemp(new BigDecimal("278.15"));
        weatherMain.setTempMax(new BigDecimal("278.15"));
        weatherMain.setTempMin(new BigDecimal("278.15"));
        weatherMain.setPressure(new BigDecimal("997"));
        weatherMain.setFeelsLike(new BigDecimal("270.01"));
        weatherMain.setHumidity(new BigDecimal("75"));

        weatherResponse = new OpenWeatherResponseDto();
        weatherResponse.setId(1L);
        weatherResponse.setMain(weatherMain);

        when(openWeatherClient.getWeather("Minsk")).thenReturn(weatherResponse);

        service = new WeatherService(openWeatherClient);

        Long actualCode = service.getIdByCityName("Minsk");
        assertThat(actualCode).isEqualTo(1L);
    }

    @Test
    public void addCity() {
        service = new WeatherService(openWeatherClient);

        City newCity = service.addCity(3, "TestCity", 1233);

        assertThat(newCity.getId()).isEqualTo(3L);
        assertThat(newCity.getName()).isEqualTo("TestCity");
        assertThat(newCity.getCityCode()).isEqualTo(1233);
    }

    @Test
    public void getCelsius() {
        service = new WeatherService(openWeatherClient);

        WeatherService.getCelsius(new BigDecimal("278.15"));
        assertThat(WeatherService.getCelsius(new BigDecimal("278.15"))).isEqualTo(new BigDecimal("5.00"));
    }

    private void verifyWeatherResponse(Weather weather) {
        assertThat(weather.getTemp()).isEqualTo(new BigDecimal("278.15"));
        assertThat(weather.getTempMax()).isEqualTo(new BigDecimal("278.15"));
        assertThat(weather.getTempMin()).isEqualTo(new BigDecimal("278.15"));
        assertThat(weather.getPressure()).isEqualTo(new BigDecimal("997"));
        assertThat(weather.getFeelsLike()).isEqualTo(new BigDecimal("270.01"));
        assertThat(weather.getHumidity()).isEqualTo(new BigDecimal("75"));
    }
}