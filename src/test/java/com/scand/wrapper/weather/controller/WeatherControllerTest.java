package com.scand.wrapper.weather.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scand.wrapper.weather.dto.CityDto;
import com.scand.wrapper.weather.dto.NewCityRequestDto;
import com.scand.wrapper.weather.model.City;
import com.scand.wrapper.weather.model.Weather;
import com.scand.wrapper.weather.service.WeatherService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(WeatherController.class)
@AutoConfigureWebClient
public class WeatherControllerTest {

    @MockBean
    private WeatherService weatherService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getCityWeatherInfo() throws Exception {
        String uri = "/weather/city/1";
        City city = new City(1, "Minsk", 5L);

        Weather weather = new Weather(
                new BigDecimal("278.15"),
                new BigDecimal("278.15"),
                new BigDecimal("278.15"),
                new BigDecimal("997"),
                new BigDecimal("270.01"),
                new BigDecimal("75"));

        given(this.weatherService.getWeather(city)).willReturn(weather);
        given(this.weatherService.findById(1)).willReturn(city);

        this.mvc.perform(get(uri))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.city.id", is(1)))
                .andExpect(jsonPath("$.city.name", is("Minsk")))
                .andExpect(jsonPath("$.city.cityCode", is(5)))
                .andExpect(jsonPath("$.weather.temp", is(5.0)))
                .andExpect(jsonPath("$.weather.feelsLike", is(5.0)))
                .andExpect(jsonPath("$.weather.tempMin", is(5.0)))
                .andExpect(jsonPath("$.weather.tempMax", is(723.85)))
                .andExpect(jsonPath("$.weather.pressure", is(270.01)))
                .andExpect(jsonPath("$.weather.humidity", is(75)));

        verify(this.weatherService).getWeather((city));
    }

    @Test
    public void getCityWeatherInfoWithCityNoyFound() throws Exception {
        String uri = "/weather/city/1";
        City city = new City(1, "Minsk", 5L);

        given(this.weatherService.findById(5)).willReturn(city);

        MvcResult mvcResult = this.mvc.perform(get(uri)).andReturn();

        assertThat(Objects.requireNonNull(mvcResult.getResolvedException()).toString()).isEqualTo("com.scand.wrapper.weather.exception.CityNotFoundException");
    }


    @Test
    public void addCity() throws Exception {
        String uri = "/weather/city/";
        NewCityRequestDto newCity = new NewCityRequestDto();
        newCity.setId(4);
        newCity.setName("NewCity");

        String inputJson = mapToJson(newCity);

        given(this.weatherService.getIdByCityName("NewCity")).willReturn(6L);

        City city = new City(6, "NewCity", 4L);
        given(this.weatherService.addCity(4, "NewCity", 6)).willReturn(city);

        MvcResult mvcResult = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertThat(200).isEqualTo(status);

        CityDto cityDtoResponse = mapFromJson(mvcResult.getResponse().getContentAsString());
        assertThat(cityDtoResponse.getCityCode()).isEqualTo(4);
        assertThat(cityDtoResponse.getName()).isEqualTo("NewCity");
        assertThat(cityDtoResponse.getId()).isEqualTo(6);
    }

    @Test
    public void addAlreadyCity() throws Exception {
        String uri = "/weather/city/";
        NewCityRequestDto newCity = new NewCityRequestDto();
        newCity.setId(4);
        newCity.setName("NewCity");

        String inputJson = mapToJson(newCity);

        given(this.weatherService.findById(4)).willReturn(new City(6, "NewCity", 4L));

        MvcResult mvcResult = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertThat(409).isEqualTo(status);
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected CityDto mapFromJson(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, CityDto.class);
    }
}