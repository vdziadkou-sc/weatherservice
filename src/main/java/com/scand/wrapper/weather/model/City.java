package com.scand.wrapper.weather.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class City {
    private long id;
    private String name;
    private long cityCode;
}
