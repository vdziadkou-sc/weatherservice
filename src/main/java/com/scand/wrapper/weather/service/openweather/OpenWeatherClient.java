package com.scand.wrapper.weather.service.openweather;

import com.scand.wrapper.weather.service.openweather.dto.OpenWeatherResponseDto;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.net.URI;

@Component
@Slf4j
@Setter
public class OpenWeatherClient {

    private static final String WEATHER_BY_ID_URL =
            "http://api.openweathermap.org/data/2.5/weather?id={city}&APPID={key}";

    private static final String WEATHER_BY_NAME_URL =
            "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}";

    private RestTemplate restTemplate;

    @Value("${app.weather.api.key}")
    private String apiKey;

    public OpenWeatherClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Cacheable("weather")
    public OpenWeatherResponseDto getWeather(long city) {
        log.info("Requesting current weather for {}", city);
        URI url = new UriTemplate(WEATHER_BY_ID_URL).expand(city, this.apiKey);
        return restTemplate.getForObject(url, OpenWeatherResponseDto.class);
    }

    public OpenWeatherResponseDto getWeather(String city) {
        log.info("Requesting current weather for {}", city);
        URI url = new UriTemplate(WEATHER_BY_NAME_URL).expand(city, this.apiKey);
        return restTemplate.getForObject(url, OpenWeatherResponseDto.class);
    }
}
