package com.scand.wrapper.weather.service.openweather.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode
public class OpenWeatherMainDto {
    private BigDecimal temp;
    @JsonProperty("feels_like")
    private BigDecimal feelsLike;
    @JsonProperty("temp_min")
    private BigDecimal tempMin;
    @JsonProperty("temp_max")
    private BigDecimal tempMax;
    private BigDecimal pressure;
    private BigDecimal humidity;

}
