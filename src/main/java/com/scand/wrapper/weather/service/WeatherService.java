package com.scand.wrapper.weather.service;

import com.scand.wrapper.weather.model.City;
import com.scand.wrapper.weather.model.Weather;
import com.scand.wrapper.weather.service.openweather.OpenWeatherClient;
import com.scand.wrapper.weather.service.openweather.dto.OpenWeatherMainDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class WeatherService {
    private final OpenWeatherClient openWeatherClient;

    private final static Map<Long, City> cities = populateDummySites()
            .stream().collect(Collectors.toMap(City::getId, c -> c));

    public City findById(long id) {
        return cities.get(id);
    }

    public Weather getWeather(City city) {
        OpenWeatherMainDto openWeatherMainDto = openWeatherClient.getWeather(city.getCityCode()).getMain();
        return new Weather(
                openWeatherMainDto.getTemp(),
                openWeatherMainDto.getFeelsLike(),
                openWeatherMainDto.getTempMin(),
                openWeatherMainDto.getTempMax(),
                openWeatherMainDto.getPressure(),
                openWeatherMainDto.getHumidity());
    }

    public Long getIdByCityName(String city) {
        return openWeatherClient.getWeather(city).getId();
    }

    public City addCity(long id, String name, long code) {
        cities.put(id, new City(id, name, code));
        log.info("City with cityId" + id + "was added");
        return new City(id, name, code);
    }

    private static final BigDecimal d = new BigDecimal("273.15");

    public static BigDecimal getCelsius(BigDecimal kelvin) {
        return kelvin.subtract(d);
    }

    private static List<City> populateDummySites() {
        List<City> cities = new ArrayList<>();

        cities.add(new City(1, "Berlin", 2950158));
        cities.add(new City(2, "Minsk", 625144));
        return cities;
    }
}
