package com.scand.wrapper.weather.service.openweather.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@EqualsAndHashCode
public class OpenWeatherResponseDto {
    private OpenWeatherMainDto main;
    private Long id;
}
