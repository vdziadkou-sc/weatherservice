package com.scand.wrapper.weather.controller;

import com.scand.wrapper.weather.dto.CityDto;
import com.scand.wrapper.weather.dto.NewCityRequestDto;
import com.scand.wrapper.weather.dto.WeatherDto;
import com.scand.wrapper.weather.dto.WeatherInfoDto;
import com.scand.wrapper.weather.exception.CityAlreadyExistException;
import com.scand.wrapper.weather.exception.CityNotFoundException;
import com.scand.wrapper.weather.model.City;
import com.scand.wrapper.weather.model.Weather;
import com.scand.wrapper.weather.service.WeatherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = "/weather/city", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class WeatherController {

    private final WeatherService weatherService;

    /**
     * Retrieve temperature in the city
     */
    @GetMapping("/{id}")
    public WeatherInfoDto getCityWeatherInfo(@PathVariable("id") long id) {
        log.info("Fetching City with id {}", id);
        City city = weatherService.findById(id);
        if (city == null) {
            log.error("city with id {} not found.", id);
            throw new CityNotFoundException();
        }

        Weather weather = weatherService.getWeather(city);

        return new WeatherInfoDto(
                new CityDto(city.getId(),
                        city.getName(),
                        city.getCityCode()),
                new WeatherDto(WeatherService.getCelsius(weather.getTemp()),
                        WeatherService.getCelsius(weather.getFeelsLike()),
                        WeatherService.getCelsius(weather.getTempMin()),
                        WeatherService.getCelsius(weather.getTempMax()),
                        weather.getPressure(),
                        weather.getHumidity()));
    }

    @PostMapping
    public CityDto addCity(@RequestBody @Valid NewCityRequestDto requestDto) {
            if (weatherService.findById(requestDto.getId()) != null)
            throw new CityAlreadyExistException();
        Long cityId = weatherService.getIdByCityName(requestDto.getName());
        City city = weatherService.addCity(requestDto.getId(), requestDto.getName(), cityId);
        return new CityDto(city.getId(), city.getName(), city.getCityCode());
    }
}