package com.scand.wrapper.weather.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CityDto {
    private long id;
    private String name;
    private long cityCode;

    public CityDto() {
    }
}
