package com.scand.wrapper.weather.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
public class WeatherDto {
    private BigDecimal temp;
    private BigDecimal feelsLike;
    private BigDecimal tempMin;
    private BigDecimal tempMax;
    private BigDecimal pressure;
    private BigDecimal humidity;
}
