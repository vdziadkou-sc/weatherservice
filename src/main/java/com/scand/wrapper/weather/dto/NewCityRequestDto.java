package com.scand.wrapper.weather.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class NewCityRequestDto {
    long id;
    @NonNull
    private String name;
}
