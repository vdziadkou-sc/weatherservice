package com.scand.wrapper.weather.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class WeatherInfoDto {
    private CityDto city;
    private WeatherDto weather;
}
